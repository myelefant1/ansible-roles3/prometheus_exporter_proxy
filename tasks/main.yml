- name: Get systemd version
  command: systemctl --version
  changed_when: false
  check_mode: false
  register: __systemd_version
  tags:
    - skip_ansible_lint

- name: Set systemd version fact
  set_fact:
    exporter_exporter_systemd_version: "{{ __systemd_version.stdout_lines[0] | regex_replace('^systemd\\s(\\d+).*$', '\\1') }}"


- name: Get latest release
  uri:
    url: "https://api.github.com/repos/QubitProducts/exporter_exporter/releases/latest"
    method: GET
    return_content: true
    status_code: 200
    body_format: json
  register: _latest_release
  until: _latest_release.status == 200
  retries: 5
  delegate_to: localhost
  become: false
  check_mode: false
  run_once: True
  when: "exporter_exporter_version is not defined"

- name: "Set exporter_exporter version to {{ _latest_release.json.tag_name[1:] }}"
  set_fact:
    exporter_exporter_version: "{{ _latest_release.json.tag_name[1:] }}"
  when: "exporter_exporter_version is not defined"

- name: Create the exporter_exporter group
  group:
    name: exporter_exporter
    state: present
    system: true

- name: Create the exporter_exporter user
  user:
    name: exporter_exporter
    group: exporter_exporter
    shell: /usr/sbin/nologin
    system: true
    create_home: false
    home: /

- name: Download exporter_exporter binary to local folder
  become: false
  get_url:
    url: "https://github.com/QubitProducts/exporter_exporter/releases/download/v{{ exporter_exporter_version }}/exporter_exporter-{{ exporter_exporter_version }}.linux-amd64.tar.gz"
    dest: "/tmp/exporter_exporter-{{ exporter_exporter_version }}.linux-amd64.tar.gz"
  register: _download_binary
  until: _download_binary is succeeded
  retries: 5
  delay: 2
  delegate_to: localhost
  check_mode: false
  run_once: True

- name: create unpack directory
  become: false
  file:
    path: "/tmp/exporter_exporter"
    state: directory
  delegate_to: localhost
  check_mode: false
  run_once: True

- name: Unpack exporter_exporter binary
  become: false
  unarchive:
    src: "/tmp/exporter_exporter-{{ exporter_exporter_version }}.linux-amd64.tar.gz"
    dest: "/tmp/exporter_exporter"
    creates: "/tmp/exporter_exporter/exporter_exporter-{{ exporter_exporter_version }}.linux-amd64/exporter_exporter"
  delegate_to: localhost
  check_mode: false
  run_once: True

- name: Propagate exporter_exporter binaries
  copy:
    src: "/tmp/exporter_exporter/exporter_exporter-{{ exporter_exporter_version }}.linux-amd64/exporter_exporter"
    dest: "/usr/local/bin/exporter_exporter"
    mode: 0755
    owner: root
    group: root
  notify: restart exporter_exporter
  when: not ansible_check_mode

- name: Copy the Exporter Exporter systemd service file
  template:
    src: exporter_exporter.service
    dest: /etc/systemd/system/exporter_exporter.service
    owner: root
    group: root
    mode: 0644
  notify: restart exporter_exporter

- name: Create Exporter Exporter main configuration file
  copy:
    content: "{{ exporter_exporter_configuration | to_nice_yaml(indent=2) | indent(2, False) }}"
    dest: /usr/local/etc/exporter_exporter.cfg
    owner: root
    group: exporter_exporter
    mode: 0440
  notify: restart exporter_exporter

- name: start exporter_exporter
  become: true
  systemd:
    daemon_reload: true
    name: exporter_exporter
    state: started
    enabled: yes
  ignore_errors: "{{ ansible_check_mode }}"
