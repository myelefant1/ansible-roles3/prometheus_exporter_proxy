# Ansible installation of prometheus exporter_exporter

Install with ansible [exporter_exporter](https://github.com/QubitProducts/exporter_exporter/releases)

## Default values

Check [default values](defaults/main.yml).

## How to test locally

```bash
./tests/bash_unit tests/tests_prometheus_exporter_proxy
```
